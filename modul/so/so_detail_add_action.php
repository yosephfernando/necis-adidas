<?php 
    include "config/config.php";
   
   $sod_product_id = $_POST['sod_product_id'];
   $sod_size = $_POST['sod_size'];
   $sod_uom_code = $_POST['sod_uom_code'];
   $sod_order_qty = $_POST['sod_order_qty'];
   $sod_unit_price = str_replace(",", "", $_POST['sod_unit_price']);
   $prm_prd_desc = $_POST['prm_prd_desc'];
   $soh_so_number = $_POST['soh_so_number'];
   $sod_grade = "01";
   
   $soHeader = "select soh_inclppn, soh_order_discount1, soh_entry_date, soh_order_ppn from SO_HEADER where soh_so_number = '$soh_so_number'";
   $rs = odbc_exec($conn, $soHeader);
   $soh_order_discount1 = odbc_result($rs, "soh_order_discount1");
   $soh_order_ppn = odbc_result($rs, "soh_order_ppn");
   $soh_entry_date = date("Y-m-d", strtotime(odbc_result($rs, "soh_entry_date")));
   $soh_inclppn = odbc_result($rs, "soh_inclppn");
   
   if($soh_inclppn == "I"){
	   $sod_total_price = ($sod_unit_price / 1.1) * $sod_order_qty;
   }else{
	   $sod_total_price = $sod_unit_price * $sod_order_qty;
   }
   
   $maxDetSeqNo = "select MAX(sod_detail_seq_no) as sod_detail_seq_no from SO_DETAIL where sod_so_number = '$soh_so_number'";
   $maxDetSeqNoRs = odbc_exec($conn, $maxDetSeqNo);
   if(odbc_result($maxDetSeqNoRs, "sod_detail_seq_no") != null){
	    $seqNo = odbc_result($maxDetSeqNoRs, "sod_detail_seq_no") + 1;
   }else{
	   $seqNo = 1;
   }
   

   function updateSoHeader($soh_so_number, $soh_order_discount1, $soh_order_ppn, $sod_total_price, $soh_inclppn, $conn){
	  if($soh_order_discount1 != 0){	
			//amount discount
		   $disc_val = ($soh_order_discount1 / 100) * ($sod_total_price);
		   $disc_final_val = $sod_total_price - $disc_val;
		   if($soh_order_ppn != 0){
			   //amount ppn
				$soh_value_ppn = ($soh_order_ppn / 100) * ($disc_final_val);   
		   }
		   
		$soh_total_netto = $disc_final_val + $soh_value_ppn; 
	
	  }
	 
	  $update = "update SO_HEADER set soh_value_discount='".round($disc_val)."', soh_value_ppn='".round($soh_value_ppn)."', soh_total_bruto='".round($sod_total_price)."', soh_total_netto='".round($soh_total_netto)."', soh_order_value='".round($soh_total_netto)."' where soh_so_number='$soh_so_number'";
	  $update = odbc_exec($conn, $update) or die(odbc_errormsg($update));
   }   

   

   if($sod_product_id != "" || $sod_size != "" || $sod_uom_code != "" || $sod_order_qty != "" || $sod_unit_price = "" || $prm_prd_desc != ""){
	   $insert_so_detail = "
		   insert into SO_DETAIL (sod_product_id, sod_size, sod_uom_code, sod_order_qty, sod_unit_price, sod_total_unit_price, sod_prod_desc, sod_so_number, sod_grade, sod_detail_seq_no, sod_uom_nama, sod_uom_isi, sod_qty_isi, sod_diskon_rate, sod_diskon_val) values ('$sod_product_id', '$sod_size', '$sod_uom_code', '$sod_order_qty', '$sod_unit_price', '$sod_total_price', '$prm_prd_desc', '$soh_so_number', '$sod_grade', '$seqNo', '', 1, '$seqNo', 0, 0);
	   ";
	   $inserTSo = odbc_exec($conn, $insert_so_detail) or die(odbc_errormsg($inserTSo));
	   
	   $sumTotalPrice = "select SUM(sod_total_unit_price) as total from SO_DETAIL where sod_so_number='$soh_so_number'";
	   $rsTotal = odbc_exec($conn, $sumTotalPrice);
	   $total = odbc_result($rsTotal, "total");
	   
	   updateSoHeader($soh_so_number, $soh_order_discount1, $soh_order_ppn, $total, $soh_inclppn, $conn);
	   echo "<script>alert('SO DETAIL ADDED');window.location.href='index.php?module=sales_order_detail_add&soh_so_number=$soh_so_number&soh_entry_date=$soh_entry_date';</script>";
   }else{
	   echo "<script>alert('DATA INVALID');window.location.href='index.php?module=sales_order_detail_add&soh_so_number=$soh_so_number&soh_entry_date=$soh_entry_date'</script>";
   }
?>