<?php 
   include "config/config.php";
   $soh_so_number = $_GET['soh_so_number'];
   $soh_entry_date = $_GET['soh_entry_date'];
   $article = $_GET['article'];
   
   $so_detail = "select sod_product_id, sod_size, sod_uom_code, sod_order_qty, sod_unit_price, sod_grade, sod_detail_seq_no from SO_DETAIL where (sod_so_number+sod_product_id+sod_grade+sod_size+CAST(sod_detail_seq_no AS VARCHAR(10))) = '$article'";
   $rs = odbc_exec($conn, $so_detail);
   
?>
<div class="col-md-12" style="padding-left:0px">
  <div class="col-md-9" style="padding-left:0px">
    <h2>Sales order detail edit</h2>
	<form action="index.php?module=sales_order_detail_edit_action" class="form-group" method="post" role="form">
	   <input type="hidden" name="article" value="<?php echo $article ?>" />
	   <input type="hidden" name="soh_so_number" value="<?php echo $soh_so_number ?>" />
	  <div class="col-md-6">
		<label>Article</label>
		<input id="article" type="text" name="sod_product_id" value="<?php echo odbc_result($rs,'sod_product_id') ?>" class="form-control" required/>
	  </div>
	  <div class="col-md-6">
		<label>Size</label>
		<input id="size" type="text" name="sod_size" class="form-control" value="<?php echo odbc_result($rs,'sod_size') ?>" required />
	  </div>
	  <div class="col-md-6">
		<label>Unit</label>
		<div id="unitAjax">
			<input id type="text" name="sod_uom_code" value="<?php echo odbc_result($rs,'sod_uom_code') ?>" class="form-control" required/>
		</div>
	  </div>
	  <div class="col-md-6">
		<label>Qty order</label>
		<input type="number" name="sod_order_qty" class="form-control" value="<?php echo number_format(odbc_result($rs,'sod_order_qty')) ?>" required />
	  </div>
	  <div class="col-md-6">
		<label>Harga</label>
		<div id="hargaAjax">
			<input type="text" name="sod_unit_price" class="form-control" value="<?php echo number_format(odbc_result($rs,'sod_unit_price')) ?>"  required/>
		</div>
	  </div>
	  <div class="col-md-12">
		<div class="clear" style="height:10px"></div>
		<button type="submit" class="btn btn-success">Update</button>
		<a href="index.php?module=sales_order_detail&soh_so_number=<?php echo $soh_so_number ?>&soh_entry_date=<?php echo $soh_entry_date ?>"><button type="button" class="btn btn-danger" >Back</button></a>
	  </div>
		
	</form>
  </div>
</div>
<script>	
	$("#article").change(function(){
	   var prd_code = $("#article").val();
	   $.ajax({url: 'index.php?module=uom_code&prd_code='+prd_code, success: function(result){
			$('#unitAjax').empty(); 
            $('#unitAjax').html(result);
        }});
		
		
	});
	
	$("#size").change(function(){
		var prd_code = $("#article").val();
		var size = $("#size").val();
		var soh_entry_date = <?php echo $soh_entry_date ?>;
		$.ajax({url: 'index.php?module=harga_prd&prd_code='+prd_code+'&size='+size+'&soh_entry_date='+soh_entry_date, success: function(result){
			$('#hargaAjax').empty(); 
            $('#hargaAjax').html(result);
        }});
	});
</script>