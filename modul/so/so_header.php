<?php 
   include "config/config.php"; 
   $customer = "select cm_cust_code1, cm_cust_name from SO_CUST_MASTER";
   $rs	= odbc_exec($conn,$customer);
   
   $rows = array();
   while($myRow = odbc_fetch_array($rs)){
		$rows[] = $myRow['cm_cust_code1'].'-'.$myRow['cm_cust_name'];
   }
   
   if(isset($_GET['from_date']) && isset($_GET['to_date']) && !isset($_GET['so_no']) && !isset($_GET['cust'])){
	   $from_date = date('Y-m-d', strtotime($_GET['from_date']));
	   $to_date = date('Y-m-d', strtotime($_GET['to_date']));

	   $soHeader = "select top 10 SH.soh_so_number, SH.soh_entry_date, SH.soh_req_delivery_date, SH.soh_cust_name, SH.soh_order_discount1, SH.soh_value_discount, SH.soh_total_bruto, SH.soh_total_netto, SM.so_sales_name, SH.soh_status_so from SO_HEADER SH join SO_SALES_MASTER SM on SH.soh_sales = SM.so_sales_id where CONVERT(char(10), SH.soh_entry_date, 126) >= '".$from_date."' and CONVERT(char(10), SH.soh_entry_date, 126) <= '".$to_date."' order by SH.soh_entry_date ASC"; 
   }else if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['so_no']) && isset($_GET['cust'])){
	   $so_no = $_GET['so_no'];
	   $cust_code = explode("-", $_GET['cust']);
	   $from_date = date('Y-m-d', strtotime($_GET['from_date']));
	   $to_date = date('Y-m-d', strtotime($_GET['to_date']));
	   
	   $soHeader = "select top 10 SH.soh_so_number, SH.soh_entry_date, SH.soh_req_delivery_date, SH.soh_cust_name, SH.soh_order_discount1, SH.soh_value_discount, SH.soh_total_bruto, SH.soh_total_netto, SM.so_sales_name, SH.soh_status_so from SO_HEADER SH join SO_SALES_MASTER SM on SH.soh_sales = SM.so_sales_id where SH.soh_so_number = '".$so_no."' and SH.soh_cust_code1 = '".$cust_code[0]."' and CONVERT(char(10), SH.soh_entry_date, 126) >= '".$from_date."' and CONVERT(char(10), SH.soh_entry_date, 126) <= '".$to_date."' order by SH.soh_entry_date ASC";
   }else if(isset($_GET['from_date']) && isset($_GET['to_date']) && !isset($_GET['so_no']) && isset($_GET['cust'])){
	   $cust_code = explode("-", $_GET['cust']);
	   $from_date = date('Y-m-d', strtotime($_GET['from_date']));
	   $to_date = date('Y-m-d', strtotime($_GET['to_date']));
	   
	   $soHeader = "select top 10 SH.soh_so_number, SH.soh_entry_date, SH.soh_req_delivery_date, SH.soh_cust_name, SH.soh_order_discount1, SH.soh_value_discount, SH.soh_total_bruto, SH.soh_total_netto, SM.so_sales_name, SH.soh_status_so from SO_HEADER SH join SO_SALES_MASTER SM on SH.soh_sales = SM.so_sales_id where SH.soh_cust_code1 = '".$cust_code[0]."' and CONVERT(char(10), SH.soh_entry_date, 126) >= '".$from_date."' and CONVERT(char(10), SH.soh_entry_date, 126) <= '".$to_date."' order by SH.soh_entry_date ASC";
   }else if(isset($_GET['from_date']) && isset($_GET['to_date']) && isset($_GET['so_no']) && !isset($_GET['cust'])){
	   $so_no = $_GET['so_no'];
	   $from_date = date('Y-m-d', strtotime($_GET['from_date']));
	   $to_date = date('Y-m-d', strtotime($_GET['to_date']));
	   
	   $soHeader = "select top 10 SH.soh_so_number, SH.soh_entry_date, SH.soh_req_delivery_date, SH.soh_cust_name, SH.soh_order_discount1, SH.soh_value_discount, SH.soh_total_bruto, SH.soh_total_netto, SM.so_sales_name, SH.soh_status_so from SO_HEADER SH join SO_SALES_MASTER SM on SH.soh_sales = SM.so_sales_id where SH.soh_so_number = '".$so_no."' and CONVERT(char(10), SH.soh_entry_date, 126) >= '".$from_date."' and CONVERT(char(10), SH.soh_entry_date, 126) <= '".$to_date."' order by SH.soh_entry_date ASC";
   }else{
	   $soHeader = "select top 10 SH.soh_so_number, SH.soh_entry_date, SH.soh_req_delivery_date, SH.soh_cust_name,SH.soh_order_discount1, SH.soh_value_discount, SH.soh_total_bruto, SH.soh_total_netto, SM.so_sales_name, SH.soh_status_so from SO_HEADER SH join SO_SALES_MASTER SM on SH.soh_sales = SM.so_sales_id order by SH.soh_entry_date DESC";
   }
   //var_dump($soHeader);die();
   
     
   $rs	= odbc_exec($conn,$soHeader);
?>
<div class="col-md-12 col-xs-12">
	<div class="col-md-12 col-xs-12 row">
		<a href="index.php?module=sales_order_header_add" class="bt_green">
				<span class="bt_green_lft"></span><strong>Add New Sales Order</strong><span class="bt_green_r"></span>
		</a>
	</div>
	<div class="col-md-12 col-xs-12 row">
	 <div class="clear" style="height:10px"></div>
	 <div class="col-md-2 col-xs-12" style="padding-left:0px">
		<input id="from_date" type="text" placeholder="SO DATE : from" class="form-control datepicker" data-date-format="d-m-yyyy" value="<?php $date = (isset($_GET['from_date'])) ? $_GET['from_date']:"";echo $date; ?>"/>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div class="col-md-2 col-xs-12" style="padding-left:0px">
		<input id="to_date" type="text" placeholder="SO DATE : to" class="form-control datepicker" data-date-format="d-m-yyyy"value="<?php $date = (isset($_GET['to_date'])) ? $_GET['to_date']:"";echo $date; ?>" />
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div class="col-md-2 col-xs-12" style="padding-left:0px">
		 <div class="input-group">
		  <input id="soNo" type="text" class="form-control" placeholder="SO NO" value="<?php $soNo = (isset($_GET['so_no'])) ? $_GET['so_no']:"";echo $soNo; ?>"/>
		</div>
	</div>
	 <div class="col-md-2 col-xs-12" style="padding-left:0px">
		 <div class="input-group">
		  <input id="cust" type="text" class="form-control" placeholder="Customer" value="<?php $cust = (isset($_GET['cust'])) ? $_GET['cust']:"";echo $cust; ?>"/>
		</div>
	</div>
	<div class="col-md-2 col-xs-12" style="padding-left:0px">
		<button id="dateRange" class="btn btn-success">Go</button>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>	
	</div>
</div>
<div class="col-md-12 col-xs-12">
 <div class="clear" style="height:10px"></div>
  <div class="table-responsive">
	<table id="rounded-corner">
		<thead>
			<tr>
				<th>#</th>
				<th>SO NO</th>
				<th>SO Date</th>
				<th>Delivery Date</th>
				<th>Customer</th>
				<th>Salesman</th>
				<th>Order discount</th>
				<th>Amount discount</th>
				<th>Total gross</th>
				<th>Total nett</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		<thead>
		
		<tbody>
		 <?php 
		 
		 if(odbc_num_rows($rs) == 0){
			echo "<h3>No data</h3>";
		 }else{
		  $i = 1; 
		  while(odbc_fetch_row($rs)){
		 ?>
			<tr>
			  <td><?php echo $i ?></td>
			  <td><a href="index.php?module=sales_order_detail&soh_so_number=<?php echo odbc_result($rs,'soh_so_number'); ?>&soh_entry_date=<?php echo date("Y-m-d", strtotime(odbc_result($rs,'soh_entry_date'))) ?>"><?php echo odbc_result($rs,'soh_so_number'); ?></a></td>
			  <td>
			  <?php 
				$date = odbc_result($rs,'soh_entry_date');
				$date = strtotime($date);
				$date = date('d M Y', $date);
				echo $date;
			  ?>
			  </td>
			  <td>
			  <?php 
				$date = odbc_result($rs,'soh_req_delivery_date');
				$date = strtotime($date);
				$date = date('d M Y', $date);
				echo $date;
			  ?>
			  </td>
			  <td><?php echo odbc_result($rs,'soh_cust_name'); ?></td>
			  <td><?php echo odbc_result($rs,'so_sales_name'); ?></td>
			  <td><?php echo number_format(odbc_result($rs,'soh_order_discount1')); ?>%</td>
			  <td><?php $val = (odbc_result($rs,'soh_value_discount') != "") ? number_format(floatval(odbc_result($rs,'soh_value_discount'))):"0";echo $val; ?></td>
			  <td><?php $val = (odbc_result($rs,'soh_total_bruto') != "") ? number_format(floatval(odbc_result($rs,'soh_total_bruto'))):"0";echo $val; ?></td>
			  <td><?php $val = (odbc_result($rs,'soh_total_netto') != "") ? number_format(floatval(odbc_result($rs,'soh_total_netto'))):"0";echo $val; ?></td>
			  <td><?php $status = (odbc_result($rs,'soh_status_so') != null) ? odbc_result($rs,'soh_status_so'):"-";echo $status; ?></td>
			  <td><img style="cursor:<?php $cursor = (odbc_result($rs,'soh_status_so') == "O") ? "pointer" : "not-allowed";echo $cursor ?>" src='images/trash.png' data-toggle="modal" data-target="#delete<?php echo odbc_result($rs,'soh_so_number'); ?>" />
			  <a href="<?php $link = (odbc_result($rs,'soh_status_so') == "O") ? "index.php?module=sales_order_header_edit&soh_so_number=".odbc_result($rs,'soh_so_number')."" : "#";echo $link ?>"><img style="cursor:<?php $cursor = (odbc_result($rs,'soh_status_so') == "O") ? "pointer" : "not-allowed";echo $cursor ?>" src='images/user_edit.png' /></a></td>
			  <?php if(odbc_result($rs,'soh_status_so') == "O"){ ?>
				  <!-- Modal -->
					<div class="modal fade" id="delete<?php echo odbc_result($rs,'soh_so_number'); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">SO <?php echo odbc_result($rs,'soh_so_number'); ?></h4>
						  </div>
						  <div class="modal-body text-center">
							<h3>Are you sure want to delete this SO ?</h3>
						  </div>
						  <div class="modal-footer">
							   <form action="index.php?module=sales_order_header_delete_action" class="form-group" method="post" role="form">
									<input type="hidden" name="soh_so_number" value="<?php echo odbc_result($rs,'soh_so_number'); ?>">
									<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
									<button type="submit" class="btn btn-primary">Yes</button>
							   </form>
						  </div>
						</div>
					  </div>
					</div>
			  <?php } ?>
			</tr>
		 <?php $i++;}} ?>
		</tbody>
	</table>
  </div>
</div>
<script>
	$('.datepicker').datepicker({
		  todayHighlight: true
	});
	
	$("#dateRange").click(function(){
		var from_date = $("#from_date").val();
		var to_date = $("#to_date").val();
		var so_no = $("#soNo").val();
		var cust = $("#cust").val();
		
		if(from_date != "" && to_date != ""){
		  if(so_no != "" && cust != ""){
			  window.location.href="index.php?module=sales_order&from_date="+from_date+"&to_date="+to_date+"&so_no="+so_no+"&cust="+cust;
		  }else if(so_no != "" && cust == ""){
			  window.location.href="index.php?module=sales_order&from_date="+from_date+"&to_date="+to_date+"&so_no="+so_no;
		  }else if(so_no == "" && cust != ""){
			  window.location.href="index.php?module=sales_order&from_date="+from_date+"&to_date="+to_date+"&cust="+cust;
		  }else{
			 window.location.href="index.php?module=sales_order&from_date="+from_date+"&to_date="+to_date; 
		  }	
		}else{	
			window.location.href="index.php?module=sales_order";
		}

	});
	
	$(function(){
		var customer = [
		 <?php foreach($rows as $row){  ?>
			'<?php 
				$row = str_Replace("'", "",$row);
				echo $row;
			?>',
		 <?php } ?>
		];
		
		$( "#cust" ).autocomplete({
		  source: customer
		});
	});
	/* $("#soNoTrigg").click(function(){
		var so_no = $("#soNo").val();
		
		if(so_no != ""){
			window.location.href="index.php?module=sales_order&so_no="+so_no;
		}else{	
			window.location.href="index.php?module=sales_order";
		}
	}); */
</script>