<?php 
    include "config/config.php";
   
   $article = $_POST['article'];
   $sod_product_id = $_POST['sod_product_id'];
   $sod_size = $_POST['sod_size'];
   $sod_uom_code = $_POST['sod_uom_code'];
   $sod_order_qty = $_POST['sod_order_qty'];
   $sod_unit_price = str_replace(",", "", $_POST['sod_unit_price']);
   $soh_so_number = $_POST['soh_so_number'];
   $sod_grade = "01";
   
   $soHeader = "select soh_inclppn, soh_order_discount1, soh_entry_date, soh_order_ppn from SO_HEADER where soh_so_number = '$soh_so_number'";
   $rs = odbc_exec($conn, $soHeader);
   $soh_order_discount1 = odbc_result($rs, "soh_order_discount1");
   $soh_order_ppn = odbc_result($rs, "soh_order_ppn");
   $soh_entry_date = date("Y-m-d", strtotime(odbc_result($rs, "soh_entry_date")));
   $soh_inclppn = odbc_result($rs, "soh_inclppn");
   
   if($soh_inclppn == "I"){
	   $sod_total_price = ($sod_unit_price / 1.1) * $sod_order_qty;
   }else{
	   $sod_total_price = $sod_unit_price * $sod_order_qty;
   }
   
   function updateSoHeader($soh_so_number, $soh_order_discount1, $soh_order_ppn, $sod_total_price, $soh_inclppn, $conn){
	  if($soh_order_discount1 != 0){	
			//amount discount
		   $disc_val = ($soh_order_discount1 / 100) * ($sod_total_price);
		   $disc_final_val = $sod_total_price - $disc_val;
		   if($soh_order_ppn != 0){
			   //amount ppn
				$soh_value_ppn = ($soh_order_ppn / 100) * ($disc_final_val);   
		   }
		   
		$soh_total_netto = $disc_final_val + $soh_value_ppn; 
	
	  }
	 
	  $update = "update SO_HEADER set soh_value_discount='".round($disc_val)."', soh_value_ppn='".round($soh_value_ppn)."', soh_total_bruto='".round($sod_total_price)."', soh_total_netto='".round($soh_total_netto)."', soh_order_value='".round($soh_total_netto)."' where soh_so_number='$soh_so_number'";
	  $update = odbc_exec($conn, $update) or die(odbc_errormsg($update));
   }   

   

   if($sod_product_id != "" || $sod_size != "" || $sod_uom_code != "" || $sod_order_qty != "" || $sod_unit_price = ""){
	   $update_so_detail = "
		   update SO_DETAIL set sod_product_id = '$sod_product_id', sod_size='$sod_size', sod_uom_code='$sod_uom_code', sod_order_qty='$sod_order_qty', sod_unit_price='$sod_unit_price', sod_total_unit_price='$sod_total_price', sod_so_number='$soh_so_number', sod_grade='$sod_grade' where (sod_so_number+sod_product_id+sod_grade+sod_size+CAST(sod_detail_seq_no AS VARCHAR(10)))='$article';
	   ";
	   $updateSo = odbc_exec($conn, $update_so_detail) or die(odbc_errormsg($updateSo));
	   
	   $sumTotalPrice = "select SUM(sod_total_unit_price) as total from SO_DETAIL where sod_so_number='$soh_so_number'";
	   $rsTotal = odbc_exec($conn, $sumTotalPrice);
	   $total = odbc_result($rsTotal, "total");
	   
	   updateSoHeader($soh_so_number, $soh_order_discount1, $soh_order_ppn, $total, $soh_inclppn, $conn);
	   echo "<script>alert('SO DETAIL UPDATED');window.location.href='index.php?module=sales_order_detail_edit&article=$article&soh_so_number=$soh_so_number&soh_entry_date=$soh_entry_date';</script>";
   }else{
	   echo "<script>alert('DATA INVALID');window.location.href='index.php?module=sales_order_detail_edit&article=$article&soh_so_number=$soh_so_number&soh_entry_date=$soh_entry_date'</script>";
   }
?>