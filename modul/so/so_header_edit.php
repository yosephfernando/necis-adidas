<?php 
   include "config/config.php";
   
   $soh_so_number = $_GET['soh_so_number'];
   
   $so_header = "select SH.soh_entry_date, SH.soh_req_delivery_date, SH.soh_cust_code1, SH.soh_cust_name, SH.soh_top, SH.soh_wh_loc_id1, SH.soh_wh_loc_id2, WH.wh_loc_name, SH.soh_sales, SM.so_sales_name, SH.soh_order_discount1, SH.soh_order_ppn, SH.soh_inclppn, SH.soh_reference from SO_HEADER SH JOIN IM_WH_LOC WH on SH.soh_wh_loc_id1+SH.soh_wh_loc_id2 = WH.wh_loc_id1+WH.wh_loc_id2 JOIN SO_SALES_MASTER SM on SH.soh_sales = SM.so_sales_id
   where SH.soh_so_number = '$soh_so_number'";
   $rs_soh_edit	= odbc_exec($conn,$so_header);
   
   $customer = "select cm_cust_code1, cm_cust_name from SO_CUST_MASTER";
   $rs	= odbc_exec($conn,$customer);
   
   $warehouse = "select wh_loc_id1, wh_loc_id2, wh_loc_name from IM_WH_LOC";
   $rs2	= odbc_exec($conn,$warehouse);
   
   $salesman = "select so_sales_id, so_sales_name from SO_SALES_MASTER";
   $rs3	= odbc_exec($conn,$salesman);
   
   $rows = array();
   while($myRow = odbc_fetch_array($rs)){
		$rows[] = $myRow['cm_cust_code1'].'-'.$myRow['cm_cust_name'];
   }
   
   $rows2 = array();
   while($myRow2 = odbc_fetch_array($rs2)){
		$rows2[] = $myRow2['wh_loc_id1'].'-'.$myRow2['wh_loc_id2'].'-'.$myRow2['wh_loc_name'];
   }
   
   $rows3 = array();
   while($myRow3 = odbc_fetch_array($rs3)){
		$rows3[] = $myRow3['so_sales_id'].'-'.$myRow3['so_sales_name'];
   }
?>
<div class="col-md-12" style="padding-left:0px">
  <div class="col-md-9" style="padding-left:0px">
    <h2>Sales order edit</h2>
	<form action="index.php?module=sales_order_header_edit_action"" class="form-group" method="post" role="form">
	  <input type="hidden" name="soh_so_number" value="<?php echo $soh_so_number ?>" />
	  <div class="col-md-6">
		<label>SO Date</label>
		<input type="text" name="soh_entry_date" class="form-control soh_entry_date" required/>
	  </div>
	  <div class="col-md-6">
		<label>Delivery Date</label>
		<input type="text" name="soh_req_delivery_date" class="form-control soh_req_delivery_date" required/>
	  </div>
	  <div class="col-md-6">
		<label>Customer</label>
		<input id="customer" type="text" name="customer" class="form-control" value="<?php echo odbc_result($rs_soh_edit,'soh_cust_code1')."-".odbc_result($rs_soh_edit,'soh_cust_name') ?>" required/>
	  </div>
	  <div class="col-md-6">
		<label>TOP</label>
		<div id="topAjax">
			<input type="text" name="soh_top" class="form-control" value="<?php echo odbc_result($rs_soh_edit,'soh_top')?>" required/>
		</div>
	  </div>
	  <div class="col-md-6">
		<label>Warehouse</label>
		<input id="warehouse" type="text" name="warehouse" class="form-control"  value="<?php echo odbc_result($rs_soh_edit,'soh_wh_loc_id1')."-".odbc_result($rs_soh_edit,'soh_wh_loc_id2')."-".odbc_result($rs_soh_edit,'wh_loc_name') ?>" required/>
	  </div>
	  <div class="col-md-6">
		<label>Salesman</label>
		<input id="salesman" type="text" name="soh_sales" class="form-control" value="<?php echo odbc_result($rs_soh_edit,'soh_sales')."-".odbc_result($rs_soh_edit,'so_sales_name') ?>" required/>
	  </div>
	  <div class="col-md-6">
			<label>DISC</label>
			<div class="input-group">
			  <input name="soh_order_discount1" type="number" class="form-control" value="<?php echo number_format(odbc_result($rs_soh_edit,'soh_order_discount1'))?>" aria-describedby="basic-addon3">
			  <span class="input-group-addon" id="basic-addon3">%</span>
			</div>
	  </div>
	  <div class="col-md-6">
	   	<div class="col-md-6" style="padding-left:0px">
			<label>PPN</label>
			<div class="input-group">
			  <input name="soh_order_ppn" type="number" class="form-control" value="10" aria-describedby="basic-addon2" readonly required/>
			  <span class="input-group-addon" id="basic-addon2">%</span>
			</div>

		</div>
		<div class="col-md-6" style="padding-right:0px">
			<label>PPN type</label>
			<select name="soh_inclppn" class="form-control" required>
			 <?php if(odbc_result($rs_soh_edit,'soh_inclppn') == "I"){ ?>
				<option value="I" selected>include</option>
				<option>exclude</option>
			 <?php }else{ ?>
				<option value="I">include</option>
				<option selected>exclude</option>
			 <?php } ?>
			</select>
		</div>
	  </div>
	  <div class="col-md-12">
		<label>Note</label>
		<textarea name="soh_reference" class="form-control"><?php echo odbc_result($rs_soh_edit,'soh_reference')?></textarea>
	  </div>
	  <!--div class="col-md-6">
		<label>Type</label>
		<input type="text" name="" class="form-control" />
	  </div-->
	  <div class="col-md-12">
		<div class="clear" style="height:10px"></div>
		<button type="submit" class="btn btn-success">Update</button>
		<a href="index.php?module=sales_order&from_date=<?php echo date('m/01/Y') ?>&to_date=<?php echo date('m/d/Y') ?>"><button type="button" class="btn btn-danger" >Back</button></a>
	  </div>
		
	</form>
  </div>
</div>
<script>
	$('.soh_entry_date').datepicker({
		  dateFormat : 'yy-mm-dd',
		  beforeShow: function() {
				setTimeout(function(){
					$('.ui-datepicker').css('z-index', 99999999999999);
				}, 0);
			}
	}).datepicker('setDate', '<?php echo date("Y-m-d",strtotime(odbc_result($rs_soh_edit,'soh_entry_date'))) ?>');
	
	$('.soh_req_delivery_date').datepicker({
		  dateFormat : 'yy-mm-dd',
		  beforeShow: function() {
				setTimeout(function(){
					$('.ui-datepicker').css('z-index', 99999999999999);
				}, 0);
			}
	}).datepicker('setDate', '<?php echo date("Y-m-d",strtotime(odbc_result($rs_soh_edit,'soh_req_delivery_date'))) ?>');
	
	$(function(){
		var customer = [
		 <?php foreach($rows as $row){  ?>
			'<?php 
				$row = str_Replace("'", "",$row);
				echo $row;
			?>',
		 <?php } ?>
		];
		
		var warehouse = [
		 <?php foreach($rows2 as $row2){  ?>
			'<?php 
				$row2 = str_Replace("'", "",$row2);
				echo $row2;
			?>',
		 <?php } ?>
		];
		
		var salesman = [
		 <?php foreach($rows3 as $row3){  ?>
			'<?php 
				$row3 = str_Replace("'", "",$row3);
				echo $row3;
			?>',
		 <?php } ?>
		];
		
		$( "#customer" ).autocomplete({
		  source: customer
		});
		
		$( "#warehouse" ).autocomplete({
		  source: warehouse
		});
		
		$( "#salesman" ).autocomplete({
		  source: salesman
		});
	});
	
	$("#customer").change(function(){
	   var cust_code = $("#customer").val();
	   var cust_code = cust_code.split('-');
	   
	   $.ajax({url: 'index.php?module=customer_code&cust_code='+cust_code[0], success: function(result){
			$('#topAjax').empty(); 
            $('#topAjax').html(result);
        }});
	});
</script>