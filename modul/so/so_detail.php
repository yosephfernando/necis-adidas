<?php 
   include "config/config.php"; 
   $soh_so_number = $_GET['soh_so_number'];
   $soh_entry_date = $_GET['soh_entry_date'];
   $soDetail = "select (SD.sod_so_number+SD.sod_product_id+SD.sod_grade+SD.sod_size+CAST(SD.sod_detail_seq_no AS VARCHAR(10))) AS article, SH.soh_status_so, SD.sod_product_id, SD.sod_so_number, SD.sod_prod_desc, SD.sod_size, SD.sod_grade, SD.sod_uom_code, SD.sod_uom_isi, SD.sod_order_qty, SD.sod_unit_price, SD.sod_total_unit_price, SD.sod_detail_seq_no from SO_DETAIL SD join SO_HEADER SH on SH.soh_so_number = SD.sod_so_number where SD.sod_so_number = '$soh_so_number'"; 

   $rs	= odbc_exec($conn,$soDetail);
?>
<div class="col-md-12 col-xs-12" style="padding-left:0px">
	<div class="col-md-3 col-xs-12">
		<a href="index.php?module=sales_order_detail_add&soh_so_number=<?php echo $soh_so_number ?>&soh_entry_date=<?php echo $soh_entry_date ?>" class="bt_green"><span class="bt_green_lft"></span><strong>Add New Sales Order Detail</strong><span class="bt_green_r"></span></a>
		<div class="clear visible-xs" style="height:10px"></div>
	</div>
</div>
<div class="col-md-12 col-xs-12" style="padding-left:0px">
  	<div class="clear" style="height:10px"></div>
  <div class="table-responsive">
	<table id="rounded-corner">
		<thead>
			<tr>
				<th>Article code</th>
				<th>Article name</th>
				<th>Grade</th>
				<th>Unit</th>
				<th>Isi</th>
				<th>Qty order</th>
				<th>Harga</th>
				<th>Sub total</th>
				<th>Action</th>
			</tr>
		<thead>
		
		<tbody>
		 <?php 
		 $i = 1;
		 while(odbc_fetch_row($rs)){ ?>
			<tr>
			  <td><?php echo odbc_result($rs,'sod_product_id'); ?></td>
			  <td><?php echo odbc_result($rs,'sod_prod_desc'); ?></td>
			  <td><?php echo odbc_result($rs,'sod_grade'); ?></td>
			  <td><?php echo odbc_result($rs,'sod_uom_code'); ?></td>
			  <td><?php echo odbc_result($rs,'sod_uom_isi'); ?></td>
			  <td><?php echo number_format(floatval(odbc_result($rs,'sod_order_qty'))); ?></td>
			  <td><?php echo number_format(floatval(odbc_result($rs,'sod_unit_price'))); ?></td>
			  <td><?php echo number_format(odbc_result($rs,'sod_total_unit_price')); ?></td>
			  <td>
				<img style="cursor:<?php $cursor = (odbc_result($rs,'soh_status_so') == "O") ? "pointer" : "not-allowed";echo $cursor ?>" src='images/trash.png' data-toggle="modal" data-target="#delete<?php echo odbc_result($rs,'sod_product_id').odbc_result($rs,'sod_size'); ?>" />
				
				<a href="<?php $link = (odbc_result($rs,'soh_status_so') == "O") ? "index.php?module=sales_order_detail_edit&article=".odbc_result($rs,'sod_so_number').odbc_result($rs,'sod_product_id').odbc_result($rs,'sod_grade').odbc_result($rs,'sod_size').odbc_result($rs,'sod_detail_seq_no')."" : "#";echo $link ?>&soh_so_number=<?php echo $soh_so_number ?>&soh_entry_date=<?php echo $soh_entry_date ?>"><img style="cursor:<?php $cursor = (odbc_result($rs,'soh_status_so') == "O") ? "pointer" : "not-allowed";echo $cursor ?>" src='images/user_edit.png' /></a>
			  </td>
			  <?php if(odbc_result($rs,'soh_status_so') == "O"){ ?>
				  <!-- Modal -->
					<div class="modal fade" id="delete<?php echo odbc_result($rs,'sod_product_id').odbc_result($rs,'sod_size'); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					  <div class="modal-dialog" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel">Article <?php echo odbc_result($rs,'sod_product_id').odbc_result($rs,'sod_size'); ?></h4>
						  </div>
						  <div class="modal-body text-center">
							<h3>Are you sure want to delete this article ?</h3>
						  </div>
						  <div class="modal-footer">
							   <form action="index.php?module=sales_order_detail_delete_action" class="form-group" method="post" role="form">
									<input type="hidden" name="article" value="<?php echo odbc_result($rs,'article'); ?>">
									<input type="hidden" name="sod_so_number" value="<?php echo odbc_result($rs,'sod_so_number'); ?>">
									<input type="hidden" name="sod_total_unit_price" value="<?php echo odbc_result($rs,'sod_total_unit_price'); ?>">
									<input type="hidden" name="soh_entry_date" value="<?php echo $soh_entry_date; ?>">
									<button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
									<button type="submit" class="btn btn-primary">Yes</button>
							   </form>
						  </div>
						</div>
					  </div>
					</div>
			  <?php } ?>
			</tr>
		 <?php $i++;} ?>
		</tbody>
	</table>
  </div>
</div>