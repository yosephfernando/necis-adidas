<script language="JavaScript">
function validateForm() {
    var uID = document.forms["frm"]["user_id"].value;
    var uName = document.forms["frm"]["user_name"].value;
    var uPwd = document.forms["frm"]["user_password"].value;
    if (uID == null || uID == "") {
        alert("Isi User ID terlebih dahulu");
        return false;
    }else if (uName == null || uName == "") {
        alert("Isi full name terlebih dahulu");
        return false;
    }else if (uPwd == null || uPwd == "") {
        alert("Isi password terlebih dahulu");
        return false;
    }
}
</script>
<div class="container">
	<h2>User Entry</h2>
    <form role="form" name="frm" onsubmit="return validateForm()" method="post" action="modul/user/save.php">
		<div class="form-group">
			<label for="user_id">User ID:</label>
			<input type="text" class="form-control" name="user_id" id="user_id" size="10">
		</div>
		<div class="form-group">
			<label for="user_name">Full Name:</label>
			<input type="text" class="form-control" name="user_name" id="user_name" size="30">
		</div>
		<div class="form-group">
			<label for="user_password">Password:</label>
			<input type="password" class="form-control" name="user_password" id="user_password">
		</div>
		<div class="form-group">
			<label for="user_div">Division:</label>
			<select class="form-control" name="user_div" id="user_div">
				<option value='SLS'>SALES</option>
				<option value='MKT'>MARKETING</option>
				<option value='PRD'>PRODUCTION</option>
				<option value='WH'>WAREHOUSE</option>
				<option value='IT'>IT</option>
			</select>
		</div>
		<div class="form-group">
			<label for="user_type">Level:</label>
			<select class="form-control" name="user_type" id="user_type">
				<option value='STAFF'>STAFF</option>
				<option value='MGR'>MANAGER</option>
				<option value='GM'>GENERAL MANAGER</option>
				<option value='DRS'>DIREKSI</option>
			</select>
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>