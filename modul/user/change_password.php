<style>
table, th, td {
    padding: 5px;
	border-spacing: 15px;
}
table {
    border-spacing: 15px;
}
</style>

<script language="JavaScript">
function validateForm() {
    var nPassword = document.forms["frm"]["new_password"].value;
    var cPassword = document.forms["frm"]["confirm_password"].value;
    if (nPassword == null || nPassword == "") {
        alert("Isi password terlebih dahulu");
        return false;
    }else if (nPassword != cPassword) {
        alert("New password & confirm password tidak sama");
        return false;
    }
}
</script>

<div class="container">
<h2>Change Password</h2>
<form role="form" name="frm" onsubmit="return validateForm()" method="post" action="index.php?module=password_update">
		<div class="form-group">
		<td><label for="user_name">User Name:</label></td>
			<input type="text" class="form-control" name="user_name" id="user_name" readonly="true" value=<?php echo $_SESSION['username']; ?>>
		</div>
		<div class="form-group">
			<label for="old_password">Old Password:</label>
			<input type="password" class="form-control" name="old_password" id="old_password">
		</div>
		<div class="form-group">
			<label for="new_password">New Password:</label>
			<input type="password" class="form-control" name="new_password" id="new_password">
		</div>
		<div class="form-group">
			<label for="confirm_password">Confirm Password:</label>
			<input type="password" class="form-control" name="confirm_password" id="confirm_password">
		</div>
		<button type="submit" class="btn btn-default">Submit</button>
</form>
</div>