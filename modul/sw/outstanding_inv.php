<?php 
   include "config/config.php";   
   $cust = "select * from SO_CUST_MASTER";
   $rs2	= odbc_exec($conn,$cust);
   
   $rows2 = array();
   while($myRow2 = odbc_fetch_array($rs2)){
		$rows2[] = $myRow2['cm_cust_code1'].'-'.$myRow2['cm_cust_name'];
   }
?>
<div class="col-md-12 col-xs-12 row">
	 <div class="clear visible-xs" style="height:10px"></div>
	 <div class="col-md-2 col-xs-12">
		<input placeholder="castomer" id="cust" type="text" name="cust" class="form-control" required/>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div class="col-md-2 col-xs-12">
		<button id="custdata" class="btn btn-success">Go</button>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>	
</div>
<div class="col-md-12 col-xs-12">
 <div class="clear" style="height:10px"></div>
  <div id="data-table" class="table-responsive">
	<table id="rounded-corner">
		<thead>
			<tr>
				<th>#</th>
				<th>INVOICENO</th>
				<th>NODO</th>
				<th>TANGGAL</th>
				<th>REF</th>
				<th>KET</th>
				<th>BROTO</th>
				<th>NET</th>
				<th>PAID</th>
				<th>STATUS</th>
			</tr>
		<thead>
		
		<tbody>
		
		</tbody>
	</table>
  </div>
</div>
<script>
	$(function(){		
		var cust = [
		 <?php foreach($rows2 as $row2){  ?>
			'<?php 
				$row2 = str_Replace("'", "",$row2);
				echo $row2;
			?>',
		 <?php } ?>
		];
		
				
		$( "#cust" ).autocomplete({
		  source: cust,
		  minLength: 3
		});
		
		
		$("#custdata").click(function(){
			var cust = $("#cust").val();
			var cust = cust.split('-');
			$('#data-table').empty(); 
			$("#data-table").append('<img src="images/squares.gif" class="img-responsive center-block" />');
			$.ajax({url: 'index.php?module=ots_inv&cust_code='+cust[0], 
					success: function(result){
						$('#data-table').empty(); 
						$('#data-table').html(result);
					}
			});
		});
	});
</script>