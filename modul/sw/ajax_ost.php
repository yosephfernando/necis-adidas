<?php 
   include "config/config.php";
   $cust_code = $_GET['cust_code'];
   $sales = $_SESSION['username']; 
   $custs = "select * from VW_AR_OUTSTANDING where KODECUST = '$cust_code' and SALES = '$sales' and STATUS = 'BELUM'";
   $custsSum = "select sum(BRUTO) as TOTALBRUTO, sum(NET) AS TOTALNET, sum(PAID) AS TOTALPAID from VW_AR_OUTSTANDING where KODECUST = '$cust_code' and SALES = '$sales' and STATUS = 'BELUM'";

   $rs4	= odbc_exec($conn,$custs);
   $rs5	= odbc_exec($conn,$custsSum);
   
    if(odbc_num_rows($rs4) == 0){
			 echo "<h3>No outstanding invoice</h3>";
	}else{
?>
		
<table class="table" id="rounded-corner">
		<thead>
			<tr>
				<th>#</th>
				<th>INVOICENO</th>
				<th>NODO</th>
				<th>TANGGAL</th>
				<th>REF</th>
				<th>KET</th>
				<th>BRUTO</th>
				<th>NET</th>
				<th>PAID</th>
				<th>STATUS</th>
			</tr>
		<thead>
		
		<tbody>
			<?php 
		 $i = 1;
		 while(odbc_fetch_row($rs4)){
			$bruto = (float)odbc_result($rs4,'BRUTO');
			$net = (float)odbc_result($rs4,'NET');
			$paid = (float)odbc_result($rs4,'PAID');
			$tanggal = date("d M Y",strtotime(odbc_result($rs4,'TANGGAL')));
		 ?>
			<tr>
			   <td><?php echo $i ?></td>
			   <td><?php echo odbc_result($rs4,'INVOICENO') ?></td>
			   <td><?php echo odbc_result($rs4,'NODO') ?></td>
			   <td><?php echo $tanggal ?></td>
			   <td><?php echo odbc_result($rs4,'REF') ?></td>
			   <td><?php echo odbc_result($rs4,'KET') ?></td>
			   <td><?php echo number_format($bruto) ?></td>
			   <td><?php echo number_format($net) ?></td>
			   <td><?php echo number_format($paid) ?></td>
			   <td><?php echo odbc_result($rs4,'STATUS') ?></td>
			</tr>
		 <?php $i++;}?>
			<tr>
				<td colspan="6" class="text-center"><b>TOTAL</b></td>
				<td ><b><?php echo number_format(odbc_result($rs5,'TOTALBRUTO')) ?></b></td>
				<td><b><?php echo number_format(odbc_result($rs5,'TOTALNET')) ?></b></td>
				<td><b><?php echo number_format(odbc_result($rs5,'TOTALPAID')) ?></b></td>
				<td></td>
			</tr>
		</tbody>
	</table>
<?php } ?>
