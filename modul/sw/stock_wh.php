<?php 
   include "config/config.php";   
   $warehouse = "select wh_loc_id1, wh_loc_id2, wh_loc_name from IM_WH_LOC";
   $rs2	= odbc_exec($conn,$warehouse);
   
   $rows2 = array();
   while($myRow2 = odbc_fetch_array($rs2)){
		$rows2[] = $myRow2['wh_loc_id1'].'-'.$myRow2['wh_loc_id2'].'-'.$myRow2['wh_loc_name'];
   }
   
   $brand = "select pl_prd_line_code, pl_prd_line_desc from IM_PRD_LINE";
   $rs3	= odbc_exec($conn,$brand);
   
   $rows3 = array();
   while($myRow3 = odbc_fetch_array($rs3)){
		$rows3[] = $myRow3['pl_prd_line_code'].'-'.$myRow3['pl_prd_line_desc'];
   }
?>
<div class="col-md-12 col-xs-12 row">
	 <div class="clear visible-xs" style="height:10px"></div>
	 <div class="col-md-2 col-xs-12">
		<input placeholder="gudang" id="warehouse" type="text" name="warehouse" class="form-control" required/>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div class="col-md-2 col-xs-12">
		<input placeholder="brand" id="brand" type="text" name="brand" class="form-control" required/>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div id="group" class="col-md-2 col-xs-12">
		<input type="text" id="groupText" name="group" placeholder="Group" class="form-control"  />
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div id="subgroup" class="col-md-2 col-xs-12">
		<input type="text" id="sgroupText" name="sgroup" placeholder="Sub group" class="form-control"  />
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
	 <div id="article" class="col-md-2 col-xs-12">
		<input type="text" id="artText" name="article" placeholder="Article" class="form-control"  />
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>
     <div class="col-md-2 col-xs-12">
		<button id="stockdata" class="btn btn-success">Go</button>
		<div class="clear visible-xs" style="height:10px"></div>
	 </div>	
</div>
<div class="col-md-12 col-xs-12">
 <div class="clear" style="height:10px"></div>
  <div id="data-table" class="table-responsive">
	<table id="rounded-corner">
		<thead>
			<tr>
				<th>#</th>
				<th>Article</th>
				<th>Grade</th>
				<th>Size</th>
				<th>AFS</th>
			</tr>
		<thead>
		
		<tbody>
		
		</tbody>
	</table>
  </div>
</div>
<script>
	$(function(){		
		var warehouse = [
		 <?php foreach($rows2 as $row2){  ?>
			'<?php 
				$row2 = str_Replace("'", "",$row2);
				echo $row2;
			?>',
		 <?php } ?>
		];
		
				
		$( "#warehouse" ).autocomplete({
		  source: warehouse,
		  minLength: 3
		});
		
		var brand = [
		 <?php foreach($rows3 as $row3){  ?>
			'<?php 
				$row3 = str_Replace("'", "",$row3);
				echo $row3;
			?>',
		 <?php } ?>
		];
		
				
		$( "#brand" ).autocomplete({
		  source: brand,
		  minLength: 3,
		  select: function (event, ui) {
			 var label = ui.item.label;
			 var brand_code = label;
			 var brand_code = brand_code.split('-');

			 $.ajax({url: 'index.php?module=brand_code&pg_prd_line_code='+brand_code[0], success: function(result){
					$('#group').empty(); 
					$('#group').html(result);
				}});
			}
		});
		
		$("#stockdata").click(function(){
			var gudang = $("#warehouse").val();
			var article = $("#artText").val();
			var brand = $("#brand").val();
			var group = $("#groupText").val();
			var sgroup = $("#sgroupText").val();
			
			var articleF = null;
			var gudangF = null;
			var brandF = null;
			var groupF = null;
			var sgroupF = null;
			
			if(gudang != ""){
				gudang = gudang.split('-');
				gudangF = gudang[0]+'-'+gudang[1];
				
				if(article != ""){
					article = article.split('-');
					articleF = article[0]+'-'+article[1]+'-'+article[2];
				}
				
				if(brand != ""){
					brand = brand.split('-');
					brandF = brand[0];
				}
				
				if(group != ""){
					group = group.split('-');
					groupF = group[0];
				}
				
				if(sgroup != ""){
					sgroup = sgroup.split('-');
					sgroupF = sgroup[0];
				}
				
				$('#data-table').empty(); 
				$("#data-table").append('<img src="images/squares.gif" class="img-responsive center-block" />');
				$.ajax({url: 'index.php?module=stock_balance&gudang_code='+gudangF+'&article='+articleF+'&brand='+brandF+'&group='+groupF+'&sgroup='+sgroupF, success: function(result){
					$('#data-table').empty(); 
					$('#data-table').html(result);
				}});
			}else{
				alert('Please fill field gudang');
			}
			
		});
	});
</script>