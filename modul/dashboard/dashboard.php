<?php
	include "config/config.php";

	$today = date("Y-m-d");
	$thismonth = date("m");
	$thisyear = date("Y");

	$total_sales_today = "
			SELECT SUM(wsh_total_so_qty) as qty_sales, SUM(wsh_net_amount) as wsh_total_so_amt_d
			FROM SO_WEB_SALES_HEADER
			WHERE wsh_loc_id1 = '".$_SESSION['username']."'
			AND wsh_so_date = '".$today."'
	";
	$total_sales_today_res = odbc_exec($conn,$total_sales_today);
	$toal_sales_today = round(odbc_result($total_sales_today_res,"qty_sales"));
	$toal_sales_today_amt = round(odbc_result($total_sales_today_res,"wsh_total_so_amt_d"));

	$total_sales_thismonth = "
			SELECT SUM(wsh_total_so_qty) as qty_sales, SUM(wsh_net_amount) as wsh_total_so_amt_m
			FROM SO_WEB_SALES_HEADER
			WHERE wsh_loc_id1 = '".$_SESSION['username']."'
			AND DATEPART(mm, wsh_so_date) = '".$thismonth."'
			AND DATEPART(YYYY, wsh_so_date) = '".$thisyear."'
	";
	$total_sales_thismonth_res = odbc_exec($conn,$total_sales_thismonth);
	$total_sales_thismonth = round(odbc_result($total_sales_thismonth_res,"qty_sales"));
	$total_sales_thismonth_amt = round(odbc_result($total_sales_thismonth_res,"wsh_total_so_amt_m"));

	$total_afs = "
			SELECT SUM(afs) as available_for_Sale
			FROM VW_STOCK_BALANCE_CONSIGMENT
			WHERE wh_id1 = '".$_SESSION['username']."'
	";
	$total_afs_res = odbc_exec($conn,$total_afs);
	$total_afs = round(odbc_result($total_afs_res, "available_for_Sale"));

	$date = new DateTime('first day of this month');
	$date->modify('first day of this month');
	$hari_pertama_bln_skrg = $date->format('Y-m-d');

	$top_10_sales_month_stats = "
	SELECT top 10 a.prd_master_code, a.available_for_Sale, a.qty_sales
	FROM (
				SELECT prd_master_code, sum(afs) as available_for_Sale, sum(wsd_qty_sales) as qty_sales
				FROM SO_WEB_SALES_HEADER
				INNER JOIN SO_WEB_SALES_DETAIL ON wsd_so_seq_no = wsh_seq_no
				INNER JOIN VW_STOCK_BALANCE_CONSIGMENT ON wsd_prd_master_code = prd_master_code and wsd_grade=prd_grade and wsd_prd_size=prd_size
				and wsh_cust_code1=wh_id1 and wsh_cust_code2=wh_id2
				WHERE wsh_loc_id1 = '".$_SESSION['username']."'
				AND wsh_so_date BETWEEN '".$hari_pertama_bln_skrg."' AND '".$today."'
				GROUP BY prd_master_code
	) a
	order by a.qty_sales desc, a.prd_master_code
	";
	$top_10_sales_month_stats_res = odbc_exec($conn,$top_10_sales_month_stats);
	$datas = array();
	$datas["wsd_prd_master_code"] = "";
	$datas["qty_sales"] = "";
	$datas["afs"] = "";
	$wsd_prd_master_code = "";
	$qty_sales = "";
	$afs = "";
	while (odbc_fetch_row($top_10_sales_month_stats_res)){
		$wsd_prd_master_code .= "'".odbc_result($top_10_sales_month_stats_res, "prd_master_code")."'".",";
		$qty_sales .= round(odbc_result($top_10_sales_month_stats_res, "qty_sales")).",";
		$afs .= round(odbc_result($top_10_sales_month_stats_res, "available_for_Sale")).",";
		$datas = array(
				"wsd_prd_master_code" => $wsd_prd_master_code,
				"qty_sales"						=> $qty_sales,
				"afs"									=> $afs,
		);
	}

	$net_sales_today_stats = "
				SELECT wsh_brand_line as brand_code, pl_prd_line_desc as brand_desc, SUM(wsh_net_amount) as net_sales
				FROM SO_WEB_SALES_HEADER
				INNER JOIN IM_PRD_LINE ON pl_prd_line_code = wsh_brand_line
				WHERE wsh_loc_id1 = '".$_SESSION['username']."'
				AND DATEPART(mm, wsh_so_date) = '".$thismonth."'
				AND DATEPART(YYYY, wsh_so_date) = '".$thisyear."'
				GROUP BY wsh_brand_line, pl_prd_line_desc
	";

	$net_sales_today_stats_res = odbc_exec($conn,$net_sales_today_stats);
	$datas_net = array();
	$datas_net["brand_desc"] = "";
	$datas_net["net_sales"] = "";
	$brand_desc = "";
	$net_sales = "";
	while (odbc_fetch_row($net_sales_today_stats_res)){
		$brand_desc .= "'".odbc_result($net_sales_today_stats_res, "brand_desc")."'".",";
		$net_sales .= round(odbc_result($net_sales_today_stats_res, "net_sales")).",";
		$datas_net = array(
				"brand_desc" => $brand_desc,
				"net_sales"	 => $net_sales
		);
	}

	$delivery_order_last_2_month = "
			SELECT obsh_shipment_no, obsh_order_booking_no, obsh_wh_pemesan1, obsh_wh_pemesan2, obsh_actual_delivery_date,
			ROW_NUMBER() OVER (Order by obsh_actual_delivery_date DESC) AS RowNumber FROM OB_SHIPMENT_HEADER
			WHERE obsh_wh_pemesan1 = '".$_SESSION['username']."'
			AND obsh_receipt_date is null
			AND obsh_actual_delivery_date >= DATEADD(MONTH, -2, GETDATE())

	";
	$delivery_order_last_2_month_res = odbc_exec($conn,$delivery_order_last_2_month);
?>
		<div class="row">
			<div class="col-md-12">
				<!-- fix for small devices only -->
				<div class="clearfix visible-sm-block"></div>

				<div class="col-md-4 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

					<div class="info-box-content">
					  <span class="info-box-text">Sales today</span>
						<span class="info-box-number"><?php echo $toal_sales_today." pcs" ?></span>
					  <span class="info-box-number"><?php echo "IDR. ".number_format($toal_sales_today_amt) ?></span>
					</div>
					<!-- /.info-box-content -->
				  </div>
				  <!-- /.info-box -->
				</div>
				<!-- /.col -->
				<div class="col-md-4 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>

					<div class="info-box-content">
					  <span class="info-box-text">Sales this month</span>
						<span class="info-box-number"><?php echo $total_sales_thismonth." pcs" ?></span>
					  <span class="info-box-number"><?php echo "IDR ".number_format($total_sales_thismonth_amt) ?></span>
					</div>
					<!-- /.info-box-content -->
				  </div>
				  <!-- /.info-box -->
				</div>
				<div class="col-md-4 col-sm-6 col-xs-12">
				  <div class="info-box">
					<span class="info-box-icon bg-green" style="background-image: url(images/icon-werehouse.png);
    background-size: contain;
    background-position-x: 5px;"></span>

					<div class="info-box-content">
					  <span class="info-box-text">Stock available for sale</span>
					  <span class="info-box-number"><?php echo number_format($total_afs)." pcs" ?></span>
					</div>
					<!-- /.info-box-content -->
				  </div>
				  <!-- /.info-box -->
				</div>
				<!-- /.col -->
			  </div>
		</div>
		<div class="col-md-6">
				  <div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Monthly sales report ( Top 10 )</h3>

					  <div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<!--div class="btn-group">
						  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-wrench"></i></button>
						  <ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						  </ul>
						</div-->

					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					  <div class="row">
						<div class="col-md-12">
						  <p class="text-center">
							<!--strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong-->
						  </p>

						  <div class="chart">
							<!-- Sales Chart Canvas -->
									<?php if( $datas["wsd_prd_master_code"] != null && $datas["qty_sales"] != null){ ?>
										<canvas id="salesChart" style="height: 500px; width: 703px;" width="703" height="500"></canvas>
									<?php
											}else{
												echo "<p>No data found</p>";
											}
									?>
						  </div>
						  <!-- /.chart-responsive -->
						</div>
						<!-- /.col -->
					  </div>
					  <!-- /.row -->
					</div>
					<!-- ./box-body -->
					<div class="box-footer">

					</div>
					<!-- /.box-footer -->
				  </div>
				  <!-- /.box -->
				<!-- /.col -->
			  </div>
			  <div class="col-md-6">
				  <div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Monthly net sales report</h3>

					  <div class="box-tools pull-right">
						<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
						</button>
						<!--div class="btn-group">
						  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown">
							<i class="fa fa-wrench"></i></button>
						  <ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						  </ul>
						</div-->

					  </div>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
					  <div class="row">
						<div class="col-md-12">
						  <p class="text-center">
							<!--strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong-->
						  </p>

						  <div class="chart">
							<!-- Sales Chart Canvas -->
							<?php if( $datas_net["brand_desc"] != null && $datas_net["net_sales"] != null){ ?>
									<canvas id="dog" style="height: 500px; width: 703px;" height="500" width="703"></canvas>
							<?php
									}else{
										echo "<p>No data found</p>";
									}
							?>
						  </div>
						  <!-- /.chart-responsive -->
						</div>
						<!-- /.col -->
					  </div>
					  <!-- /.row -->
					</div>
					<!-- ./box-body -->
					<div class="box-footer">

					</div>
					<!-- /.box-footer -->
				  </div>
				  <!-- /.box -->
			  </div>
			  <div class="col-md-12">
				<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Goods in transit</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive table-striped">
                <table class="table no-margin table-striped" id="rounded-corner" style="width: 100%;">
                  <thead>
                  <tr>
										<th>#</th>
                    <th>No transaksi</th>
                    <th>Pemesan</th>
										<th>Shipment Date</th>
                  </tr>
                  </thead>
                  <tbody>
										<?php while (odbc_fetch_row($delivery_order_last_2_month_res)){
											$actual_date = strtotime(odbc_result($delivery_order_last_2_month_res, "obsh_actual_delivery_date"));
											$actual_date = date("d M Y", $actual_date);
											$pemesan = odbc_result($delivery_order_last_2_month_res, "obsh_wh_pemesan1")."-".odbc_result($delivery_order_last_2_month_res, "obsh_wh_pemesan2");
										?>
			                  <tr>
													<td><?php echo odbc_result($delivery_order_last_2_month_res, "RowNumber") ?></td>
			                    <td><a id="trig<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>" href="#" data-toggle="modal" data-target="#<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>"><?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?></a></td>
													<script>
															$("#trig<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>").click(function(){
																var code = "<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>";
																$.ajax({
																	url: "index.php?module=ajax_detail&obsd_shipment_no=" + code,
																}).done(function(data) {
																		$("#content<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>").empty();
																		$("#content<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>").html(data);
																});
															});
													</script>
													<td><?php echo $pemesan ?></td>
			                    <td><?php echo $actual_date ?></td>
			                  </tr>
												<!-- Modal -->
												<div class="modal fade" id="<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												  <div class="modal-dialog" role="document">
												    <div class="modal-content">
												      <div class="modal-header">
												        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												        <h4 class="modal-title" id="myModalLabel">No Transaksi <?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?></h4>
												      </div>
												      <div id="content<?php echo odbc_result($delivery_order_last_2_month_res, "obsh_order_booking_no") ?>" class="modal-body">
																	<img src="images/ajax.loader.gif" class="img-responsive center-block">
												      </div>
												      <div class="modal-footer">
												        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												      </div>
												    </div>
												  </div>
												</div>
										<?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">

            </div>
            <!-- /.box-footer -->
          </div>
			  </div>
	  <script>
	  <?php if( $datas["wsd_prd_master_code"] != null && $datas["qty_sales"] != null){ ?>
			var ctx = document.getElementById("salesChart").getContext("2d");
			var data = {
						labels: [
							<?php echo $datas["wsd_prd_master_code"] ?>
						],
						datasets: [
							{
								label: "Qty sales",
								backgroundColor:[
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
									'#2E2EFE',
								],
								data: [<?php echo $datas["qty_sales"] ?>]
							},
							{
								label: "Available for sale",
								backgroundColor:[
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
									'#60c8f2',
								],
								data: [<?php echo $datas["afs"] ?>]
							}
					]
			};
			var salesChart = new Chart(ctx, {
			    type: 'bar',
			    data: data,
					options : {
			     scales: {
			         yAxes: [{
			             ticks: {
			                 beginAtZero: true,
			                 userCallback: function(label, index, labels) {
			                     // when the floored value is the same as the value we have a whole number
			                     if (Math.floor(label) === label) {
			                         return label;
			                     }

			                 },
			             }
			         }],
			     },
				 responsive: true
			 }
			});
  <?php } ?>
	<?php if( $datas_net["brand_desc"] != null && $datas_net["net_sales"] != null){ ?>
			var ctx2 = document.getElementById("dog").getContext("2d");
			var data_day = {
						labels: [
							<?php echo $datas_net["brand_desc"] ?>
						],
						datasets: [
							{
								label: "Net sales",
								backgroundColor:[
								 <?php
								 	$count = explode( ",", $datas_net["net_sales"]);
								 	$count = count($count);
									function get_random_color()
									{
											$c = "";
											for ($i = 0; $i<6; $i++)
									    {
									        $c .=  dechex(rand(0,15));
									    }
									    return "#$c";
									}
									for($i = 1;$i <= $count;$i++){
								 ?>
										'<?php echo get_random_color();?>',
								 <?php } ?>
								],
								data: [<?php echo $datas_net["net_sales"] ?>]
							}
						],


			};


			var myPieChart = new Chart(ctx2,{
			    type: 'doughnut',
			    data : data_day,
				options: {
					tooltips: {
						enabled: true,
						mode: 'single',
						callbacks: {
								label: function(tooltipItem, data) {
									var allData = data.datasets[tooltipItem.datasetIndex].data;
									var tooltipLabel = data.labels[tooltipItem.index];
									var tooltipData = allData[tooltipItem.index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");;
									return tooltipLabel + ' : ' + tooltipData;
								}


							}
						}
					},
					responsive: true
			});
	<?php } ?>
</script>
