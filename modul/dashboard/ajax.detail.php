<?php
  include "config/config.php";
  $no_ship = $_GET['obsd_shipment_no'];
  $detail_do = "
  SELECT obsd_prd_master_code, SUM(obsd_actual_qty_shipped) as obsd_actual_qty_shipped
  FROM OB_SHIPMENT_DETAIL
  WHERE obsd_shipment_no = '".$no_ship."'
  GROUP BY obsd_prd_master_code
  ";
  $detail_do_res = odbc_exec($conn,$detail_do);
?>
<table class="table no-margin table-striped" id="rounded-corner" style="width: 100%;">
  <thead>
  <tr>
    <th>Article</th>
    <th>Ship qty</th>
  </tr>
<?php while (odbc_fetch_row($detail_do_res)){ ?>
  <tr>
    <td><?php echo odbc_result($detail_do_res, "obsd_prd_master_code") ?></td>
    <td><?php echo odbc_result($detail_do_res, "obsd_actual_qty_shipped") ?></td>
  </tr>
<?php } ?>
  </thead>
</table>
