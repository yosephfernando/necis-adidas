<?php ob_start(); ?>
<?php
//date_default_timezone_set('Asia/Jakarta');
//date_default_timezone_set('America/New_York');
session_start();
if(!isset($_SESSION['username'])) {
	header('location:login.php');
}else{
	$user_login = $_SESSION['username'];
	$user_level = $_SESSION['usertype'];
	//$user_div = $_SESSION['userdiv'];
}
if(isset($_GET['module']) && $_GET['module'] == "customer_code"){
	include "modul/so/ajax_custcode.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "uom_code"){
	include "modul/so/ajax_unit.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "harga_prd"){
	include "modul/so/ajax_harga.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "brand_code"){
	include "modul/sw/ajax_group.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "group_code"){
	include "modul/sw/ajax_sgroup.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "art_code"){
	include "modul/sw/ajax_artcode.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "stock_balance"){
	include "modul/sw/ajax_sb.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "ots_inv"){
	include "modul/sw/ajax_ost.php";
	die();
}
/* if(isset($_GET['module']) && $_GET['module'] == "ajax_detail"){
	include "modul/dashboard/ajax.detail.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "artc_ajax"){
	include "modul/rolling/artc_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "add_action"){
	include "modul/rolling/add_action.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "temporer_ajax"){
	include "modul/rolling/temporer_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "delete_temporer_ajax"){
	include "modul/rolling/delete_temporer_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "list_ajax"){
	include "modul/rolling/list_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "add_action_retur"){
	include "modul/retur/add_action.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "temporer_ajax_retur"){
	include "modul/retur/temporer_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "delete_temporer_ajax_retur"){
	include "modul/retur/delete_temporer_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "list_ajax_retur"){
	include "modul/retur/list_ajax.php";
	die();
}else if(isset($_GET['module']) && $_GET['module'] == "list_ajax_catalog"){
	include "modul/master/list_ajax.php";
	die();
} */
?>
<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
  <head>
	<link rel="shortcut icon" href="images/SDI.ico"/>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
	<link rel="stylesheet" href="css/table.css" type="text/css">
	<script language="javascript" type="text/javascript" src="niceforms.js"></script>
	<link rel="stylesheet" type="text/css" media="all" href="niceforms-default.css" />
	<script type="text/javascript" src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!--<script type="text/javascript" src="js/jquery.min.js"></script>-->
	<script type="text/javascript" src="js/jconfirmaction.jquery.js"></script>
	<script type="text/javascript" src="plugins/datepicker/bootstrap-datepicker.js"></script>
	<link rel="stylesheet" href="plugins/datepicker/datepicker3.css" />
	<script type="text/javascript" src="js/highcharts.js"></script>
	<script type="text/javascript" src="js/modules/exporting.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>

 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SDI Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
    -->
    <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">
	<link rel="stylesheet" href="css/jquery-ui.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <!--
  BODY TAG OPTIONS:
  =================
  Apply one or more of the following classes to get the
  desired effect
  |---------------------------------------------------------|
  | SKINS         | skin-blue                               |
  |               | skin-black                              |
  |               | skin-purple                             |
  |               | skin-yellow                             |
  |               | skin-red                                |
  |               | skin-green                              |
  |---------------------------------------------------------|
  |LAYOUT OPTIONS | fixed                                   |
  |               | layout-boxed                            |
  |               | layout-top-nav                          |
  |               | sidebar-collapse                        |
  |               | sidebar-mini                            |
  |---------------------------------------------------------|
  -->
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="index.php" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Home</b></span>
        </a>

        <!-- Header Navbar -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <!-- Menu toggle button -->

                <ul class="dropdown-menu">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the messages -->
                    <ul class="menu">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <!-- User Image -->
                            <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                          </div>
                          <!-- Message title and timestamp -->
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <!-- The message -->
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                    </ul><!-- /.menu -->
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li><!-- /.messages-menu -->

              <!-- Notifications Menu -->

              <!-- Tasks Menu -->
              <li class="dropdown tasks-menu">
                <!-- Menu Toggle Button -->
                <ul class="dropdown-menu">
                  <li class="header">You have 9 tasks</li>
                  <li>
                    <!-- Inner menu: contains the tasks -->
                    <ul class="menu">
                      <li><!-- Task item -->
                        <a href="#">
                          <!-- Task title and progress text -->
                          <h3>
                            Design some buttons
                            <small class="pull-right">20%</small>
                          </h3>
                          <!-- The progress bar -->
                          <div class="progress xs">
                            <!-- Change the css width attribute to simulate progress -->
                            <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                              <span class="sr-only">20% Complete</span>
                            </div>
                          </div>
                        </a>
                      </li><!-- end task item -->
                    </ul>
                  </li>
                  <li class="footer">
                    <a href="#">View all tasks</a>
                  </li>
                </ul>
              </li>
              <!-- User Account Menu -->
              <li class="dropdown user user-menu">
                <!-- Menu Toggle Button -->
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- The user image in the navbar-->
                  <img src="images/sdi.ico" class="user-image" alt="User Image">
                  <!-- hidden-xs hides the username on small devices so only the image appears. -->
                  <span class="hidden-xs"><?php echo $user_login;?></span>
                </a>
                <!--ul class="dropdown-menu">

                  <li class="user-header">
                    <img src="dist/img/admin.jpg" class="img-circle" alt="User Image">
                    <p>
                      Administrator
                      <small>Member</small>
                    </p>
                  </li>


                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a href="#" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul-->
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- Sidebar user panel (optional) -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="images/sdi.ico" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p><?php echo $user_login;?></p>
              <!-- Status -->
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <!-- search form (Optional) -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->

		  <?php
			if((isset($_GET["module"])) && ($_GET["module"] != "")){
				$mod = $_GET["module"];
			}else{
				$mod = "";
			}
		  ?>

          <!-- Sidebar Menu -->

          <ul class="sidebar-menu">
			 <!--li class="header">HEADER</li-->
			 <!--<li id="dash"><a href="index.php?module=dashboard"><i class="fa fa-link"></i> <span>DASHBOARD</span></a></li>-->
			 <?php if($user_login == "admin"){ ?>
				<!--<li id="dash"><a href="index.php?module=dashboard"><i class="fa fa-link"></i> <span>DASHBOARD</span></a></li>-->
			 <?php } ?>
			 <!--li id="master"><a href="index.php?module=master"><i class="fa fa-link"></i> <span>Master</span></a></li>
			 <li id="consigment" class="treeview">
				  <a href="#"><i class="fa fa-link"></i> <span>Consigment</span> <i class="fa fa-angle-left pull-right"></i></a>
				  <ul class="treeview-menu">
					 <li><a href="index.php?module=consigment">Consigment Entry</a></li>
					 <li><a href="index.php?module=report_sales&from_date=&to_date=&counter=&name=">Report Sales</a></li>
				  </ul>
			 </li-->
			 <li id="inventory" class="treeview">
				  <a href="#"><i class="fa fa-link"></i> <span>Inventory</span> <i class="fa fa-angle-left pull-right"></i></a>
				  <ul class="treeview-menu">
					  <li id="stock_wh"><a href="index.php?module=stock_wh">Stock gudang</a></li>
				  </ul>
			 </li>
			 <li id="sales" class="treeview">
				  <a href="#"><i class="fa fa-link"></i> <span>Sales</span> <i class="fa fa-angle-left pull-right"></i></a>
				  <ul class="treeview-menu">
					  <li id="sales_order"><a href="index.php?module=sales_order&from_date=<?php echo date('m/01/Y') ?>&to_date=<?php echo date('m/d/Y') ?>">Sales order</a></li>
					  <li id="ost_inv"><a href="index.php?module=ost_inv">Outstanding invoice</a></li>
				  </ul>
			 </li>
			 <?php if($user_login == "admin"){ ?>
					<!--li id="setting_dash">
						<a href="index.php?module=user"><i class="fa fa-link"></i> <span>Settings Dashboard</span></a>
					</li-->
					<li id="user_list">
						<a href="index.php?module=user"><i class="fa fa-link"></i> <span>User</span></a>
					</li>
			 <?php } ?>
			<li id="change_password"><a href="index.php?module=change_password"><i class="fa fa-link"></i> <span>Change Password</span></a></li>
			<li><a href="logout.php"><i class="fa fa-link"></i> <span>Logout</span></a></li>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
          </ol>
        </section>
		<section class="container">
        <!-- Main content -->

		<?php
			if((isset($_GET["module"])) && ($_GET["module"] != "")){
				$mod = $_GET["module"];
			}else{
				$mod = "";
			}

			switch($mod){
				/* case "dashboard":
					include "modul/dashboard/image.php";
					echo "
						<script>
							$('#dash').addClass('active');
						</script>
					";
				break;
				case "master":
					include "modul/master/list.php";
					echo "
						<script>
							$('#master').addClass('active');
						</script>
					";
				break;
				
				//Begin Consigment------------------------------
				case "consigment":
					include "modul/consigment/list.php";
					echo "
						<script>
							$('#consigment').addClass('active');
						</script>
					";
				break;
				case "cons_date":
					include "modul/consigment/cons_date.php";
					echo "
						<script>
							$('#consigment').addClass('active');
						</script>
					";
				break;
				case "cons_event":
					include "modul/consigment/cons_event.php";
					echo "
						<script>
							$('#consigment').addClass('active');
						</script>
					";
				break;
				case "cons_entry":
					include "modul/consigment/cons_entry.php";
					echo "
						<script>
							$('#consigment').addClass('active');
						</script>
					";
				break;
				//End Consigment--------------------------------------
				
				case "report_stock":
					include "modul/report/report_stock.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				//Begin Receipt DO-----------------
				case "receiptdo":
					include "modul/receiptdo/list.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				case "receipt_entry":
					include "modul/receiptdo/receipt_entry.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				case "receipt_add":
					include "modul/receiptdo/receipt_add.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				//End Receipt DO
				//Begin List DO------------------------------------
				case "listdo":
					include "modul/listdo/list.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				case "listdodetail":
					include "modul/listdo/detaildo.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				//End List DO------------------------------------
				case "report_sales":
					include "modul/report/report_sales.php";
					echo "
						<script>
							$('#consigment').addClass('active');
						</script>
					";
				break;
				//Begin Rolling------------------------------------
				case "rolling_entry":
					include "modul/rolling/list.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				case "rolling_entry_add":
					include "modul/rolling/add.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				//End Rolling------------------------------------
				
				//Begin Return------------------------------------
				case "return_entry":
					include "modul/retur/list.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				case "return_entry_add":
					include "modul/retur/add.php";
					echo "
						<script>
							$('#inventory').addClass('active');
						</script>
					";
				break;
				//End Return------------------------------------
				
				case "setting_dash":
					include "modul/admin/setting_dash.php";
					echo "
						<script>
							$('#setting_dash').addClass('active');
						</script>
					";
				break; */
				//Begin SO-------------------------------------
				case "sales_order":
					include "modul/so/so_header.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_header_add":
					include "modul/so/so_header_add.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_header_add_action":
					include "modul/so/so_header_add_action.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_header_edit":
					include "modul/so/so_header_edit.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_header_edit_action":
					include "modul/so/so_header_edit_action.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_detail":
					include "modul/so/so_detail.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_detail_add":
					include "modul/so/so_detail_add.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_detail_add_action":
					include "modul/so/so_detail_add_action.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_detail_edit":
					include "modul/so/so_detail_edit.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_detail_edit_action":
					include "modul/so/so_detail_edit_action.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_header_delete_action":
					include "modul/so/so_header_delete_action.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				case "sales_order_detail_delete_action":
					include "modul/so/so_detail_delete_action.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#sales_order').addClass('active');
						</script>
					";
				break;
				//End SO---------------------------------------
				
				//begin stock_wh
				case "stock_wh":
					include "modul/sw/stock_wh.php";
					echo "
						<script>
							$('#inventory').addClass('active');
							$('#stock_wh').addClass('active');
						</script>
					";
				break;
				case "ost_inv":
					include "modul/sw/outstanding_inv.php";
					echo "
						<script>
							$('#sales').addClass('active');
							$('#ost_inv').addClass('active');
						</script>
					";
				break;
				//end stock wh
				
				//Begin User-----------------------------------
				case "user":
					include "modul/user/list.php";
					echo "
						<script>
							$('#user_list').addClass('active');
						</script>
					";
				break;
				case "user_add":
					include "modul/user/add.php";
				break;
				case "ubah_password":
					include "ubah_password.php";
					echo "
						<script>
							$('#ubah_password').addClass('active');
						</script>
					";
				break;
				//End User-----------------------------------
				
				//Begin Chage Paswword-----------------------------------
				case "change_password":
					include "modul/user/change_password.php";
					echo "
						<script>
							$('#change_password').addClass('active');
						</script>
					";
				break;
				case "password_update":
					include "modul/user/password_update.php";
				break;
				//End Chage Paswword-----------------------------------
				default:
					include "modul/dashboard/image.php";
					echo "
						<script>
							$('#master').addClass('active');
						</script>
					";
					//include "modul/dashboard/dashboard.php";
					//echo "
					//	<script>
					//		$('#dash').addClass('active');
					//	</script>
					//";
				break;
			}
		?>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          Suryadata Infokreasi
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a target= "blank_" href="http://www.suryadata.com">Suryadata Infokreasi</a>.</strong> All rights reserved.
      </footer>


      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="dist/js/app.min.js"></script>
  </body>
</html>
<?php ob_flush(); ?>